

myvariable = f'I am variable myvariable from module {__name__}'

def say_hello():
    print(f'Hello! I am function say_hello from module {__name__} ')

print(f'This is an executable statement from module {__name__}')