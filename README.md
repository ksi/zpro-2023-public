# ZPRO 2023

Tato stránka bude obsahovat veškeré informace týkající se výuky předmětu 18ZPRO – oznámení, materiály ke cvičení, apod.

Obsah tohoto projektu můžete jednoduše synchronizovat do prostředí [JupyterHub](https://jupyter.fjfi.cvut.cz/) kliknutím na následující tlačítko:

[![Static Badge](https://img.shields.io/badge/Synchronizovat%20na%20JupyterHub-08c?logo=jupyter&labelColor=white&color=08c)](
https://jupyter.fjfi.cvut.cz/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgitlab.fjfi.cvut.cz%2Fksi%2Fzpro-2023-public.git&urlpath=lab%2Ftree%2Fzpro-2023-public.git%2FREADME.md&branch=main)

## Oznámení

1. Ve čtvrtek 19. října v čase 10:00–11:40 budou skupiny z učeben T-115 a T-101 spojeny v učebně T-101. Všichni prosím přineste vlastní notebook!
2. Na stránce https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-resene-priklady najdete řešené příklady ze cvičení.
3. Hodnocení druhého úkolu bylo 22. října upraveno kvůli příliš přísné automatické kontrole.
   Bodování stručně: 50 bodů za správný výpočet, 10 bodů za zaokrouhlení (i špatných) výsledků, 40 bodů za dodržení formátu výpisu „X d Y hod“.
   Použijte tlačítko **Fetch Feedback** v seznamu úkolů, abyste si stáhli aktuální hodnocení!
4. V pátek 10. listopadu a v pondělí 13. listopadu bude skupina z učebny T-115 v čase 8:00–9:40 spojena s ostatními skupinami ve stejném čase.
   Studenti s vlastním notebookem se přesunou do posluchárny T-101 a studenti, kteří nemají možnost přinést vlastní notebook, se přesunou do učebny T-105.
5. Termín pro odevzdání 5. úkolu bude prodloužen **do úterý 14. 11.** s ohledem na problémy s přihlašováním na JupyterHub.
   Zadání úkolů jsou zkopírované také ve složce [úkoly](./úkoly/), aby byly k dispozici i bez přihlášení.
   **JupyterHub již funguje, vypracované úkoly odevzdávejte opět tam.**
   Omlouváme se za komplikace a zhoršené podmínky během tohoto týdne.
6. V týdnu 20.11. - 24.11. se na cvičení bude psát první test.
   Vyučující každé skupiny určí, jestli test bude na prvním nebo druhém cvičení v tomto týdnu.
   Standardní doba na vypracování bude 15 minut a bude obsahovat 10-15 jednoduchých otázek a úkolů z témat, která jsme dosud stihli probrat.
   Ukázkovou verzi otázek najdete v souboru [testy/demo1.pdf](testy/demo1.pdf)
   Test bude mít papírovou formu a nebude při něm možné používat jakékoliv elektronické pomůcky.
7. V týdnu 18.12. - 21.12. se na cvičení bude psát druhý test.
   Forma testu bude v podstatě stejná jako v prvním případě, ukázkovou verzi otázek najdete v souboru [testy/demo2.pdf](testy/demo2.pdf).
8. V pondělí 18. prosince v čase 8:00–9:40 bude skupina z učebny T-105 rozptýlena do učeben T-115 (studenti, kteří nemají vlastní notebook) a T-101 (studenti s vlastním notebookem).

## Odkazy

- [Bílá kniha](https://bilakniha.cvut.cz/cs/predmet11274505.html) – osnova, doplňující literatura
- [GitLab projekt](https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-public)
- [Úvodní přednáška z Přípravného týdne](00_PT.pdf), [dotazník](https://forms.gle/7vNS1Y2a2ESgm34r7)
- [Informace o uživatelském účtu FJFI](https://it.fjfi.cvut.cz/component/flexicontent/clanky/uzivatelsky-ucet)
- [Fakultní JupyterHub](https://jupyter.fjfi.cvut.cz/)
- [Anonymní prostředí Jupyter](https://jupyter.org/try-jupyter)
- [Řešené příklady ze cvičení](https://gitlab.fjfi.cvut.cz/ksi/zpro-2023-resene-priklady)

## Autoři

Na přípravě těchto materiálů se podíleli:

- Jakub Klinkovský
- Zuzana Petříčková
- Vladimír Jarý
- Maksym Dreval
- Petr Pauš
- Jan Tomsa
- František Voldřich

Materiály jsou dostupné pod licencí [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
