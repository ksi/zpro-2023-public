Úkoly zde jsou zkopírovaná zadání z JupyterHubu, aby byly k dispozici alespoň read-only.

Vypracované úkoly bude nutné odevzdat opět na JupyterHubu, až bude opět funkční přihlašování – termín bude s ohledem na problémy prodloužen.
Mailem nebo jinými způsoby nám to neposílejte 🙂
